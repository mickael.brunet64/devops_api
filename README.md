# Prequerisites

To build an API Platform project, you'll nedd to download and extract :

- The API Platform Distribution : https://github.com/api-platform/api-platform/releases
<h1 align="center"><a href="https://api-platform.com"><img src="https://api-platform.com/logo-250x250.png" alt="API Platform"></a></h1>

and install : 

- VSCode : https://code.visualstudio.com/download

- Git : https://git-scm.com/downloads

You might need Docker to run the dockerfile file in the repository

- Docker Desktop for Windows : https://docs.docker.com/docker-for-windows/install/

> Docker is using server side deamon to run containers. Make sure WSL is installed and upated to version 2

> Docker Desktop for linux is not available

## Install and clone the project

Once extracted, the directory containing the files of the API project must be cloned to a distant repository. Use GitLab

1. **Git Configuration**

`git config --global user.name "usermane"`

`git config --global user.email "email"`

> The **global** parameter is used to set the same configuration for all the repositories

2. **Initialise the project**

`git init`

3. **Clone the project**

Two ways to clone a repository :

* Via the git command shell :

`git clone https://gitlab.com/username/repository.git`

* By cloning it directly from your project in GitLab :
    1. Cloning with SSH
    2. Cloning with Https

You can now open a new project in VSCode by cloning the repository and work directly on an IDE.

After editing the code, you can commit the changes to your branch (if you are not commiting to the main branch) by tiping :

`git add -A`

> the **"A"** parameter helps you not specify files to commit, it takes all the changes

`git commit -m "your commit"`

> the **"-m"** parameter is here to write a message alongside your commit

Finally you can push the changes to the distant repository with the command **push** :

`git push`

