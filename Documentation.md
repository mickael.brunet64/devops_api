# Projet API Platform et pipeline d’intégration continue Gitlab

*Avant de commencer :*
*Cette documentation a pour but de permettre la mise en place d’un projet API Platform et de mettre en place un premier pipeline d’intégration continue à l’aide de GitLab.*

## **API Platform**

API Platform est un outil écrit en PHP et basé sur le framework Symfony. Il permet de créer facilement et rapidement une API.

Pour mettre en place le projet :

- Télécharger le dossier zippé depuis GitHub : https://github.com/api-platform/api-platform/releases
Une fois le dossier dézippé, on peut utiliser son IDE préféré pour ouvrir le dossier

## **Gitlab, Github et Git**

**GitLab** est une plateforme permettant d’héberger et gérer des projets web. Au niveau fonctionnel, GitLab a une palette plus large que Github. GitLab nous permet de faire de la gestion de dépôt de code source mais aussi d’intégrer des logiques de développement et d’amélioration continue, mettre en place du monitoring ainsi qu’effectuer une sécurité applicative. C’est enfin une plateforme collaborative, open source et gratuite.

**Github** nous permet aussi de faire de la gestion de dépôt de code source et est similaire à Gitlab. Seulement il ne possède pas les fonctionnalités citées plus haut.

**Git** est un logiciel de gestion de versions décentralisé. Il permet de gérer plusieurs projets et en les envoyant sur un serveur distant. Toute personnes qui travaille sur un projet est connecté avec les autres. Il offre une synchronisation continue.

Lors de la création d’un projet sur Github ou bien Gitlab, le developpeur télécharge et installe Git. Les téléchargements selon les différents systèmes d’exploitation sont disponibles à cette adresse :
https://git-scm.com/download

A l’ouverture de la console Git, la première étape afin d’utiliser le service et de configurer un compte utilisateur. Pour cela il est necéssaire d’entrer un nom d’utilisateur et une adresse e-mail
Voici les commandes nécessaires :

`Git config –global user.name « nom d’utilisateur »`

`Git config –global user.email « adresse e-mail »`

La seconde étape consiste à transformer un dossier en un dossier git. Cette étape est une étape importante avant de pouvoir utiliser les commandes add et commit que l’on va voir par la suite.

`#make this directory a git directory` <br>
`Git init`

Tout les fichiers créés ont besoin d'être ajouté au dossier git et la commande **add** va nous permettre d'ajouter dans le dossier.

`git add <fichier>` <br>
`git add -A`

> Le paramètre -A nous permet d'indiquer à git que nous souhaitons englober tous les changements.

L'étape suivante est l'étape du commit. Le commit est l'enregistrement des changements : 

`git commit -m "message"`

> Le paramètre -m permet d'associer un message au commit.

Pour envoyer sur le dépôt distant les modifications faites, on utilise la commande **push**

`git push`

Maintenant, si nous avons besoin de mettre à jour le dépôt local, c'est à dire par exemple lors de changement sur plusieurs branches (un developpeur qui a effectué des changements) il existe la commande **pull** comme ceci :

`git pull`

## Gestion des branches

Pour créer une branche depuis l'invite de commande git nous utiliserons <br>
`git branch <new branch>`

A l'inverse si on souhaite la supprimer on tapera <br> 
`git branch -d <branch>`

Pour changer de branche et ajouter et commit sur celle ci on tapera <br>
`git checkout <branch>`

Une dernière commande qui peut s'avérer utile et interessante est la commande **log** elle nous permet de lister toute l'activité sur la branche sur laquelle on se trouve.

# Intégration d'une pipeline CI

Tout d'abord qu'est ce que l'intégration continue ?

L'intégration continue est un ensemble de pratiques utilisées en génie logiciel consistant à vérifier, à chaque modification de code source que le résultat des modifications ne produit pas de régréssion dans l'application developpée.

Cette intégration peut se faire en 5 étape selon les besoins d'automatisation. Voici un exemple :

- Planifier le developpement
- Compiler et intégrer le code
- Tester le code
- Mesurer la qualité du code
- Gerer les livrables de l'application

La plateforme Gitlab permet par exemple de mettre en place des pipeline. Voici un exemple simple (sans script) d'une pipeline permettant de compiler, effectuer des tests unitaire et déployer. Lors de chaque remontée de code, la pipeline s'activera et fera passer les tests au code permettant de le valider.

```
stages:

  - build
  - test
  - deploy

build-job: 

  stage: build

  script:
    - echo "Compiling the code..."
    - echo "Compile complete."

unit-test-job:

  stage: test

  script:
    - echo "Running unit tests... This will take about 60 seconds."
    - sleep 60
    - echo "Code coverage is 90%"

lint-test-job:

  stage: test

  script:
    - echo "Linting code... This will take about 10 seconds."
    - sleep 10
    - echo "No lint issues found."

deploy-job:

  stage: deploy

  script:
    - echo "Deploying application..."
    - echo "Application successfully deployed."
```

La syntaxe utilisé par Git est la syntaxte YALM et porte l'extension .yml <br>
Un fichier **.gitlab-ci.yml** sera ajouté au dépôt. C'est dans celui ci que l'on pourra éditer le pipeline.

Pour structurer une pipeline nous avons besoin de mot-clés. Ces mots clés vont nous permettre d'une part d'y voir plus clair et d'autre part de respecter la syntaxe.

- **Stages** : ce sont les étapes du pipeline. Dans l'exemple plus haut il existe 3 étapes **build** , **test** et **deploy**

- **job** : ce sont les tâches du pipeline. Chaque étape contient une tâche. Dans ces tâches se trouve tous les scripts necéssaires à la compilation, aux tests et au déploiement. Ici nous avons **build-job**, **unit-test-job**, **lint-test-job** et enfin **deploy-job** <br>
Dans chaque tâche nous rapellons l'étape à laquelle on se trouve. 

- **script** : ce sont les fichiers qui contiennent le code qui permet de compiler, tester et déployer.

> Si une erreur est détectée, peu importe l'étape à laquelle on se trouve, le pipeline s'arrête et les étapes suivantes sont ignorées (skipped) <br>
Inversement, si le pipeline fonctionne correctement jusqu'au dernier stage il est inscrit "passed"


![Jobs](jobs.PNG)

Pour un projet essentiellement écrit en PHP tel que le projet API Platform, des logiciels comme PHPUnit peuvent nous aider à créer des scripts de test unitaire. Gitlab recense des templates de pipeline que l'on peut utiliser selon le language de notre projet, nous pouvons les retrouver à l'adresse : https://docs.gitlab.com/ee/ci/examples/README.html#gitlab-cicd-examples

## La création d'environnement

Une fonctionnalité de Gitlab qui peut être implémentée dans un pipeline est la création d'environnement de production. Si notre environnement de déploiement est basé sur un serveur distant hebergé sur un Cloud par exemple, nous pouvons ajouter cet environnement directement dans la section **Deployments** > **Environments**. Dans cette partie j'ai choisi d'intégrer une **machine virtuelle EC2** crée au préalable grâce aux services AWS

![Live environment](deployments.PNG)

Pour ajouter un environnement, il suffit de cliquer sur **New environment**, entrer un nom et une URL externe afin de pouvoir accéder en direct à l'environnement de déploiement.

![New environment](New_environment.PNG)

Sur le site d'AWS, une fois un compte créé, voici les étapes pour créer une instance de EC2 :

![Etape 1](devops.PNG)

![Etape 2](devops_instance.PNG)

Il ne reste plus qu'à cliquer sur créer.

# Prise en main de Docker

## Qu'est ce que Docker ?

Docker est une technologie de conteneurisation qui permet la création et l'utilisation de conteneur Linux. Cette technologie permet de s'affranchir des problèmes liés aux dépendances et à la compatibilé de l'application présente dans le conteneur. On peut les voir comme des machines virtuelles legères et modulaires. Ces conteneurs permettent notamment d'optimiser les applications pour le Cloud, ce qui se définit comme une bonne pratique DevOps.

Docker nous propose de télécharger **Docker Desktop** qui nous permet d'avoir un visuel sur les conteneurs, les images et les volumes. Avec une offre payante, il est possible d'explorer les fichiers que contiennent les volumes.

### Télécharger une image

Les conteneurs se servent d'images que l'on peut trouver sur le **Docker hub**

![Docker Hub](DockerHub.PNG)

Pour déployer un conteneur Ubuntu on peut taper <br>
`docker run -i -t ubuntu bash`

A ce moment, si l'image n'est pas présente localement, docker la télécharge. Cette manipulation est comprise par Docker comme si on avait tapé manuellement la commande <br>
`docker pull ubuntu`

Ensuite docker crée un conteneur de nouveau comme si on avait tapé la commande <br>
`docker container create`

Dans cet exemple, les paramètres **-t** ainsi que **-i** indique que le conteneur fonctionne de façon intéractive avec le terminal et le résultat des commandes se trouve envoyé aussi sur le terminal. 

![Conteneurs](containers.PNG)

> Si on ne donne pas de nom à l'image que l'on conteneurise, elle prend un nom donné par Docker par défaut.

Un autre exemple avec l'image **hello-world** disponible sur le hub docker

![Conteneurs](hello-world.PNG)

## La gestion de projet et le suivi utilisateur

Une des nombreuses fonctionnalités que Gitlab possède est la gestion de projet avec l'onglet **Issue** et **board** <br>

Il peut être interéssant de créer des tableaux sur lesquels on crée des libellés. Cela peut être par exemple un board "Developpement" ou bien "Backlog" pour les principaux objectifs à developper.

![Issue Board](issue-board.PNG)

Pour finir, la partie **service desk** nous permet aussi de rester en contact avec les utilisateurs du service, car c'est eux qui en parle le mieux.






